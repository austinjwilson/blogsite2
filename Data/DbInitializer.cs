﻿using BlogSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSite.Data
{
    public class DbInitializer
    {
        public static void Initialize(BlogContext context)
        {
            context.Database.EnsureCreated();

            //Look for any blog
            if (context.Blogs.Any())
            {
                return;
            }

            var blogs = new Blog[]
            {
            new Blog{Title="1st Blog Post",Author="Austin",Article="This is my first blog post",Date= DateTime.Today}
            };
            foreach (Blog b in blogs)
            {
                context.Blogs.Add(b);
            }
            context.SaveChanges();
        }

    }
}

